using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using trab_crud.Models.DB;
using trab_crud.Data;

namespace trab_crud.Controllers
{
    public class ContatoController : Controller
    {
        private readonly ILogger<ContatoController> _logger;

        private readonly appContext _context;

        public ContatoController(ILogger<ContatoController> logger, appContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {
            IList<Contato> contatos = _context.Contatos.ToList();
            return View();
        }

    }
}